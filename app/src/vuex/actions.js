import * as types from './mutation-types'
import uuid from 'node-uuid'
import EcdsaSrv from './services/EcdsaSrv'
import fs from 'fs'

export const addKey = ({ commit }, {curve, username, email, password}) => {
  EcdsaSrv.generateKey(curve).then(function (key) {
    key.uuid = uuid.v4()
    key.username = username
    key.email = email
    commit(types.ADD_KEY, key)
    commit(types.STOP_GENERATING_KEY)
  })
}

export const removeKey = ({ commit }, uuid) => {
  commit(types.REMOVE_KEY, uuid)
}

export const chooseKey = ({ commit }, uuid) => {
  commit(types.CHOOSE_KEY, uuid)
}

export const startGeneratingKey = ({commit}) => {
  commit(types.START_GENERATING_KEY)
}

export const stopGeneratingKey = ({commit}) => {
  commit(types.STOP_GENERATING_KEY)
}

export const signText = ({commit}, {privateKey, text}) => {
  EcdsaSrv.sign(privateKey, text).then(function (signature) {
    commit(types.SIGN, signature)
  })
}

export const signFile = ({commit}, {privateKey, filepath}) => {
  fs.readFile(filepath, function (err, data) {
    if (err) {
      console.log(err)
    } else {
      console.log(data)
      EcdsaSrv.sign(privateKey, new Uint8Array(data)).then(function (signature) {
        console.log(signature)
        commit(types.SIGN, signature)
      })
    }
  })
}
