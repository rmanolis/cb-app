import * as types from '../mutation-types'
import _ from 'lodash'
import Vue from 'vue'

const state = {
  signature: ''
}

const mutations = {
  [types.SIGN] (state, signature) {
    state.signature = signature
  },
  [types.CLEAN_SIGNATURE] (state) {
    state.signature = ''
  }
}

export default {
  state,
  mutations
}
