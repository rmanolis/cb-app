import * as types from '../mutation-types'
import _ from 'lodash'
import Vue from 'vue'

const state = {
  keys: [],
  choosenKey: null,
  isGeneratingKey: false
}

const mutations = {
  [types.ADD_KEY] (state, key) {
    state.keys.push(key)
  },
  [types.REMOVE_KEY] (state, uuid) {
    const index = state.keys.findIndex(x => x.uuid === uuid)
    state.keys.splice(index, 1)
    state.choosenKey = null
  },
  [types.CHOOSE_KEY] (state, uuid) {
    state.keys.forEach(function (key) {
      if (key.uuid === uuid) {
        Vue.set(state, 'choosenKey', key)
      }
    })
  },
  [types.START_GENERATING_KEY] (state) {
    state.isGeneratingKey = true
  },
  [types.STOP_GENERATING_KEY] (state) {
    state.isGeneratingKey = false
  }
}

export default {
  state,
  mutations
}
