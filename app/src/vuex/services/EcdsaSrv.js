import cblibjs from 'cblibjs'

export default class EcdsaSrv {
  static generateKey (curve) {
    return new Promise(function (resolve, reject) {
      const [key, error] = cblibjs.GenerateKeys(curve)
      if (error) {
        reject(error)
      } else {
        resolve({publicKey: key.PublicKey,
        privateKey: key.PrivateKey})
      }
    })
  }
  static sign (privateKey, text) {
    return new Promise(function (resolve, reject) {
      const [signature, error] = cblibjs.Sign(privateKey, text)
      if (error) {
        reject(error)
      } else {
        resolve(signature)
      }
    })
  }
  
  static verify (publicKey, signature, text) {
    return new Promise(function (resolve, reject) {
      const [isCorrect, error] = cblibjs.Verify(publicKey, text, signature)
      if (error) {
        reject(error)
      } else {
        resolve(isCorrect)
      }
    })
  }
  static verifyFile (publicKey, signature, file) {
    return new Promise(function (resolve, reject) {
      const [isCorrect, error] = cblibjs.VerifyFile(publicKey, file, signature)
      if (error) {
        reject(error)
      } else {
        resolve(isCorrect)
      }
    })
  }
  static encrypt (publicKey, text) {
    return new Promise(function (resolve, reject) {
      const [encText, error] = cblibjs.Encrypt(publicKey, text)
      if (error) {
        reject(error)
      } else {
        resolve(encText)
      }
    })
  }

  static decrypt (privateKey, text) {
    return new Promise(function (resolve, reject) {
      const [decText, error] = cblibjs.Decrypt(privateKey, text)
      if (error) {
        reject(error)
      } else {
        resolve(decText)
      }
    })
  }

}
