// getters for profiles
export const keys = state => state.profiles.keys
export const choosenKey = state => state.profiles.choosenKey
export const isGeneratingKey = state => state.profiles.isGeneratingKey
export const getKey = state => uuid => {
  state.profiles.forEach((profile) => {
    if (profile.uuid === uuid) {
      return profile
    }
  })
  return null
}

// getters for signature_form
export const getSignature = state => state.signature_form.signature
