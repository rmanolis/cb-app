// mutate profiles
export const ADD_KEY = 'ADD_KEY'
export const REMOVE_KEY = 'REMOVE_KEY'
export const CHOOSE_KEY = 'CHOOSEN_KEY'
export const START_GENERATING_KEY = 'START_GENERATING_KEY'
export const STOP_GENERATING_KEY = 'STOP_GENERATING_KEY'

// mutate signature_form
export const SIGN = 'SIGN'
export const CLEAN_SIGNATURE = 'CLEAN_SIGNATURE'
