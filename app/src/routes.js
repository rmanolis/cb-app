export default [
  {
    path: '/',
    name: 'keys-page',
    component: require('components/KeysPageView')
  },
  {
    path: '/choosen/key',
    name: 'key-page',
    component: require('components/KeyPageView')
  },
  {
    path: '/choosen/key/sign',
    name: 'sign-page',
    component: require('components/SignPageView')
  },
  {
    path: '/choosen/key/verify',
    name: 'verify-page',
    component: require('components/VerifyPageView')
  },
  {
    path: '/choosen/key/encrypt',
    name: 'encrypt-page',
    component: require('components/EncryptPageView')
  },
  {
    path: '/choosen/key/decrypt',
    name: 'decrypt-page',
    component: require('components/DecryptPageView')
  }

]
